const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const presetEnv = require("postcss-preset-env");
const autoprefixer = require("autoprefixer");

const templatePath = 'path/to/my/template' // e.g. wp-content/themes/buro26

module.exports = env = {
    mode: 'production',
    entry: {
        main: path.resolve(__dirname, `${templatePath}/ts/index.tsx`),
        styles: path.resolve(__dirname, `${templatePath}/scss/styles.scss`),
        vendors: path.resolve(__dirname, `${templatePath}/scss/vendors.scss`),
    },
    devtool: 'inline-source-map',
    output: {
        publicPath: `/${templatePath}/dist/`,
        filename: 'js/[name].[hash].bundle.js',
        chunkFilename: 'js/[name].[hash].bundle.js',
        path: path.resolve(__dirname, `${templatePath}/dist/`),
    },
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 20000,
            minChunks: 1,
            maxAsyncRequests: 30,
            maxInitialRequests: 30,
            enforceSizeThreshold: 30000,
            cacheGroups: {
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                    name: 'js_vendors',
                    reuseExistingChunk: true,
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true,
                },
            },
        },
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.scss' ],
    },
    module: {
        rules: [
            {
                // Fix for framer motion v5: https://github.com/framer/motion/issues/1307#issuecomment-960991171
                test: /\.mjs$/,
                type: 'javascript/auto',
                include: /node_modules/,
            },
            {
                test: [/\.tsx?$/,  /\.jsx?$/],
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                "@babel/preset-typescript",
                                {
                                    allowNamespaces: true,
                                }
                            ],
                            "@babel/preset-react",
                            [
                                "@babel/preset-env",
                                {
                                    modules: false,
                                    targets: {
                                        esmodules: true,
                                    },
                                },
                            ],
                        ],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                            "@babel/plugin-syntax-dynamic-import",
                        ],
                    },
                },
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre",
            },
            {
                test: /\.(s[ac]ss)$/,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: false,
                            sourceMap: true,
                            publicPath: (resourcePath, context) => {
                                return '/'
                            },
                        },
                    },
                    // Translates CSS into CommonJS
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                        },
                    },
                    // Resolve urls
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    // Compiles Sass to CSS
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                outputStyle: 'compressed',
                            },
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                parser: 'postcss-scss',
                                plugins: [
                                    "autoprefixer",
                                    presetEnv({}),
                                    autoprefixer({
                                        grid: 'autoplace',
                                    }),
                                ],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: false,
                            sourceMap: true,
                            publicPath: (resourcePath, context) => {
                                return '/'
                            },
                        },
                    },
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                parser: 'postcss-scss',
                                plugins: [
                                    "autoprefixer",
                                    presetEnv({}),
                                    autoprefixer({
                                        grid: 'autoplace',
                                    }),
                                ],
                            },
                        },
                    },
                ],
            },
            {
                // File loader for images
                test: /\.(svg|jpg|jpeg|png)(?:[?#].+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: `/${templatePath}/dist/`,
                            name: `[name].[ext]`,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'css/[name].[hash].css',
            chunkFilename: 'css/[name].[hash].css',
        }),
        new CleanWebpackPlugin({
            dry: false,
        }),
    ],
};